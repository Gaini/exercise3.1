var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://localhost:27017/taskMongo';

MongoClient.connect(url, function (err, db) {
   if (err) {
       console.log('невозможно подключиться к серверу MongoDB. Ошибка: ', err);
   } else {
        console.log('Соединение установлено для ', url);

        var collection = db.collection('users');

        var user1 = {name: 'Gaini', age: '30'};
        var user2 = {name: 'Sergey', age: '30'};
        var user3 = {name: 'Nastya', age: '24'};

        collection.insertMany([user1, user2, user3], function (err, result) {
            if (err) {
                console.log(err);
            } else {
                console.log(result.ops);
            }
            db.close();
        });

       collection.updateMany({age: '30'}, { $set: {'name': 'Azamat'}}, function(err, result){
           if (err) {
               console.log(err);
           } else {
               if (result) {
                   console.log('Обновление успешно');
                   console.log(result);
               } else {
                   console.log('Не найдены документы для обновления');
               }
           }
           db.close();
       });

       collection.find().toArray(function(err, result){
           if (err) {
               console.log(err);
           } else {
               console.log('После обновления имен: ');
               console.log(result);
           }
           db.close();
       });

       collection.deleteMany({name: "Azamat"}, function(err, result){
           if (err) {
               console.log(err);
           } else {
               console.log('Удаление успешно')
               console.log(result);
           }
           db.close();
       });

       collection.find().toArray(function(err, result){
           if (err) {
               console.log(err);
           } else {
               console.log('После удаления имен: ');
               console.log(result);
           }
           db.close();
       });

   }
});